FROM python:3.8

# Copy in the Python module requirements and install.
WORKDIR /project
COPY requirements.txt .
ARG PIP_NO_CACHE_DIR=0
RUN pip install -r requirements.txt

# Copy in the module and install.
# Copy in the root module and install.
ARG MODULE_DIR=dagster_dask_ext
COPY $MODULE_DIR $MODULE_DIR
COPY setup.py .
ARG SETUP_COMMAND=install
RUN python setup.py ${SETUP_COMMAND}
