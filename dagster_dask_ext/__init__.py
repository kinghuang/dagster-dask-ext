from .version import __version__


try:
    import dagster_aws_ext
    _enable_glue_support = True
except:
    _enable_glue_support = False
