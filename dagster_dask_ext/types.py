from dagster import (
    # Definition
    Field, EventMetadataEntry, Failure, Materialization,

    # Types
    Any, List, String, dagster_type_loader, dagster_type_materializer, SerializationStrategy,

    # Config
    Permissive, Selector, Shape,

    # Type Creation
    DagsterType, PythonObjectDagsterType
)
from dagster.core.storage.system_storage import fs_system_storage
from dagster.core.storage.type_storage import TypeStoragePlugin
from dagster.utils import frozenlist
from dask import dataframe as dd

from . import _enable_glue_support


DataFramePathType = Any  # String, List[String]
ReadCSVIncludePathColumnType = Any  # Boolean, String
ReadParquetFiltersType = Any  # List[Tuple[str, str, Any]], List[List[Tuple[str, str, Any]]]
ReadParquetIndexType = Any  # List, False or None
ToCSVHeaderType = Any  # Boolean, List[String]
ToCVSIndexLabelType = Any  # String, List[String]
ToParquetCompressionType = Any  # String, Dict


DataFrameSerDeTypes = {
    "csv": {
        "read_function": dd.read_csv,
        "read_options": {
            "include_path_column": Field(ReadCSVIncludePathColumnType, is_required=False, description="Whether or not to include the path to each particular file."),
        },
        "to_function": dd.DataFrame.to_csv,
        "to_options": {
            "single_file": Field(bool, is_required=False, description="Whether to save everything into a single CSV file."),
            "compression": Field(str, is_required=False, description="String like ‘gzip’ or ‘xz’."),
            "sep": Field(str, is_required=False, description="Field delimiter for the output file."),
            "na_rep": Field(str, is_required=False, description="Missing data representation."),
            "float_format": Field(str, is_required=False, description="Format string for floating point numbers."),
            "columns": Field([str], is_required=False, description="Columns to write."),
            "header": Field(ToCSVHeaderType, is_required=False, description="Write out column names."),
            "header_first_partition_only": Field(bool, is_required=False, description="Only write the header row in the first output file."),
            "index": Field(bool, is_required=False, description="Write row names (index)."),
            "index_label": Field(ToCVSIndexLabelType, is_required=False, description="Column label for index column(s) if desired."),
        },
        "glue_serde": "UNKNOWN",
    },
    "parquet": {
        "read_function": dd.read_parquet,
        "read_options": {
            "columns": Field([str], is_required=False, description="Fields names to read in as columns."),
            "filters": Field(ReadParquetFiltersType, is_required=False, description="List of filters to apply."),
            "index": Field(ReadParquetIndexType, is_required=False, description="Field name(s) to use as the output frame index."),
        },
        "to_function": dd.DataFrame.to_parquet,
        "to_options": {
            "compression": Field(ToParquetCompressionType, is_required=False, description="Column compression."),
            "write_index": Field(bool, is_required=False, description="Whether or not to write the index."),
            "partition_on": Field([str], is_required=False, description="Construct directory-based partitioning by splitting on these fields’ values."),
            "write_metadata_file": Field(bool, is_required=False, description="Whether to write the special “_metadata” file."),
        },
        "glue_serde": "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe",
    },
    "json": {
        "read_function": dd.read_json,
        "read_options": {
            "orient": Field(str, is_required=False, description="Indication of expected JSON string format."),
            "lines": Field(bool, is_required=False, description="Read the file as a json object per line."),
        },
        "to_function": dd.DataFrame.to_json,
        "to_options": {
            "orient": Field(str, is_required=False, description="Indication of expected JSON string format."),
            "lines": Field(bool, is_required=False, description="Write out line delimited json format."),
            "compression": Field(str, is_required=False, description="String like ‘gzip’ or ‘xz’."),
            "index": Field(bool, is_required=False, description="Whether to include the index values in the JSON string."),
        },
        "glue_serde": "org.openx.data.jsonserde.JsonSerDe",
    }
}


def _dataframe_loader_config():
    read_sources = {
        "base": {
             "path": Field(DataFramePathType, description="Path to read from."),
        }
    }

    if _enable_glue_support:
        read_sources["glue"] = {
            "database": Field(String, is_required=False, description="Glue database to read from."),
            "table": Field(String, description="Glue table to read from."),
        }

    read_fields = Selector({
        read_type if read_source == "base" else f"{read_type}_{read_source}": {**source_opts, **type_meta["read_options"]}
        for read_type, type_meta in DataFrameSerDeTypes.items() for read_source, source_opts in read_sources.items()
    })

    return Shape({
        "read": read_fields,
        "sample": Field(float, is_required=False, description="Fraction of items to return."),
        "repartition": Field(Selector({
            "npartitions": Field(int, description="Number of partitions of output."),
            "partition_size": Field(Any, description="Max number of bytes of memory for each partition."),
        }), is_required=False, description="Repartition dataframe along new divisions."),
        "reset_index": Field(bool, is_required=False, description="Reset the index to the default index"),
        "lower_cols": Field(bool, is_required=False, description="Lowercase column names."),
    })


@dagster_type_loader(_dataframe_loader_config())
def _dataframe_loader(context, config):
    read_type, type_opts = next(iter(config["read"].items()))
    type_opts = dict(type_opts)

    if read_type.endswith("glue"):
        read_type = read_type.split("_")[0]
        database, table = type_opts.pop("database"), type_opts.pop("table")

        # Get a read_path from the table's StorageDescriptor
        table_info = context.resources.glue.get_table(database=database, table=table)
        storage_desc = table_info["StorageDescriptor"]
        read_path = storage_desc["Location"]

        # Check the table SerDe for consistency with the read_type
        serde_lib = storage_desc["SerdeInfo"]["SerializationLibrary"]
        expected_lib = DataFrameSerDeTypes[read_type]["glue_serde"]
        if expected_lib != serde_lib:
            context.log.warning(f"The Glue table serialization library {serde_lib} does not match {expected_lib} expected for {read_type}.")
    else:
        read_path = type_opts.pop("path")
    assert read_path is not None

    read_function = DataFrameSerDeTypes[read_type]["read_function"]
    df = read_function(read_path, **type_opts)

    if opts := config.get("sample", None):
        df = df.sample(frac=opts)

    if opts := config.get("repartition", None):
        df = df.repartition(**opts)

    if opts := config.get("lower_cols", False):
        df.columns = map(str.lower, df.columns)

    if opts := config.get("reset_index", False):
        df = df.reset_index()
    
    return df


def _dataframe_materializer_config():
    to_destinations = {
        "base": {
            "path": Field(DataFramePathType, description="Path to write to."),
        }
    }

    # if _enable_glue_support:
    if False:
        to_destinations["glue"] = {
            "database": Field(String, is_required=False, description="Glue database to write to."),
            "table": Field(String, description="Glue table to write to."),
            "path": Field(DataFramePathType, is_required=False, description="Path to write to."),
        }
    
    to_fields = {
        to_type if to_destination == "base" else f"{to_type}_{to_destination}": Field({**destination_opts, **type_meta["to_options"]}, is_required=False)
        for to_type, type_meta in DataFrameSerDeTypes.items() for to_destination, destination_opts in to_destinations.items()
    }

    return Shape({
        "to": to_fields,
        "sample": Field(float, is_required=False, description="Fraction of items to return."),
        "repartition": Field(Selector({
            "npartitions": Field(int, description="Number of partitions of output."),
            "partition_size": Field(Any, description="Max number of bytes of memory for each partition."),
        }), is_required=False, description="Repartition dataframe along new divisions."),
        "reset_index": Field(bool, is_required=False, description="Reset the index to the default index"),
    })


@dagster_type_materializer(_dataframe_materializer_config())
def _dataframe_materializer(context, config, df):
    if opts := config.get("sample", None):
        df = df.sample(frac=opts)

    if opts := config.get("repartition", None):
        df = df.repartition(**opts)

    if opts := config.get("reset_index", False):
        df = df.reset_index()

    for to_type, type_opts in config["to"].items():
        type_opts = dict(type_opts)
        type_opts = {key: list(value) if isinstance(value, frozenlist) else value for key, value in type_opts.items()}

        if to_type.endswith("glue"):
            # Not supported yet.
            pass
        else:
            to_path = type_opts.pop("path")
        assert to_path is not None

        to_function = DataFrameSerDeTypes[to_type]["to_function"]
        materialization_future = to_function(df, to_path, compute=False, **type_opts)
        materialization_result = context.resources.dask.client.compute(materialization_future, sync=True)

        yield Materialization(
            to_type,
            f"DataFrame materialized as {to_type} data.",
            [
                EventMetadataEntry.path(path=to_path, label=f"{to_type} path", description=f"{to_type} data.")
            ]
        )


class DaskDataFrameParquetFilesystemAndS3StoragePlugin(TypeStoragePlugin):
    @classmethod
    def compatible_with_storage_def(cls, system_storage_def):
        if system_storage_def is fs_system_storage:
            return True
        else:
            try:
                from dagster_aws.s3 import s3_system_storage
                return system_storage_def is s3_system_storage
            except ImportError:
                return False
        return False
    
    @classmethod
    def set_object(cls, intermediate_store, obj, context, dagster_type, paths):
        target_path = intermediate_store.key_for_paths(paths)
        obj.to_parquet(intermediate_store.uri_for_paths(paths), compression="snappy")
        return target_path
    
    @classmethod
    def get_object(cls, intermediate_store, context, dagster_type, paths):
        return dd.read_parquet(intermediate_store.uri_for_paths(paths))


class DaskDataFramePickleSerializationStrategy(SerializationStrategy):
    def __init__(self, name="dataframe_pickle"):
        super(DaskDataFramePickleSerializationStrategy, self).__init__(name)

    def serialize(self, value, write_file_obj):
        df = value.compute()
        df.to_pickle(write_file_obj)

    def deserialize(self, read_file_obj):
        import pandas as pd
        
        df = pd.read_pickle(read_file_obj)
        return dd.from_pandas(df, npartitions=1)


DataFrame = PythonObjectDagsterType(
    python_type=dd.DataFrame,
    name="DaskDataFrame",
    description="A Dask DataFrame.",
    loader=_dataframe_loader,
    materializer=_dataframe_materializer,
    serialization_strategy=DaskDataFramePickleSerializationStrategy(),
    # auto_plugins=[DaskDataFrameFilesystemAndS3StoragePlugin],
    required_resource_keys={"dask"},
)
