from setuptools import find_packages, setup


def get_version():
    version = {}
    with open('dagster_dask_ext/version.py') as fp:
        exec(fp.read(), version)

    return version['__version__']


install_requires = [
    "dagster",
    "dagster-dask",
    "dask",
    "dask[dataframe]",
]


dependency_links = [

]


setup(
    name='dagster-dask-ext',
    version=get_version(),
    description='Extensions on the dagster-dask library module.',
    author='King Chung Huang',
    author_email='king.huang@enverus.com',
    packages=find_packages(),
    package_data={},
    install_requires=install_requires,
    dependency_links=dependency_links,
    zip_safe=True
)
